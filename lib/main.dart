import 'package:flutter/material.dart';
import 'components/http_service.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Http Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HttpPage(title: 'Http Demo'),
    );
  }
}
