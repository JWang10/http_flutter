import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart';

class HttpPage extends StatefulWidget {
  const HttpPage({
    Key key,
    @required this.title,
  }) : super(key: key);

  final String title;

  @override
  _HttpPageState createState() => _HttpPageState();
}

class _HttpPageState extends State<HttpPage> {
  bool _loading = false;
  String _text = "";

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    API api = new API();

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Title(
            color: Colors.white,
            child: Text(widget.title),
          ),
        ),
        body: ConstrainedBox(
          constraints: BoxConstraints.expand(),
          child: Column(
            children: [
              ElevatedButton(
                onPressed: _loading
                    ? null
                    : () async {
                        setState(() {
                          _loading = true;
                          _text = "Query...";
                        });
                        try {
                          // await api.getGet().then((value) => _text = value);
                          await api.getPost().then((value) => _text = value);
                        } catch (e) {
                          _text = "Error: $e";
                        } finally {
                          setState(() {
                            _loading = false;
                          });
                        }
                      },
                child: Text(
                  "Get Message",
                ),
              ),
              Container(
                width: size.width - 50.0,
                child: Text(_text),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class API {
  Uri uri = Uri.http("127.0.0.1:5000", "/Test");

  Future<String> getGet() async {
    Response res = await get(uri);
    if (res.statusCode == 200) {
      return res.body;
    } else {
      return "Error: " + res.body;
    }

    // // http get without http package
    // HttpClient httpClient = new HttpClient();
    // HttpClientRequest request =
    //     await httpClient.get("127.0.0.1", 5000, "/Test");
    // HttpClientResponse response = await request.close();
    // Future<String> responseBody = response.transform(utf8.decoder).join();
    // httpClient.close();
    // if (response.statusCode == 200) {
    //   return responseBody;
    // } else {
    //   return "Error: " + await responseBody;
    // }
  }

  Future<String> getPost() async {
    // String payload = "...";
    // HttpClient httpClient = new HttpClient();

    // HttpClientRequest request = await httpClient.getUrl(uri);

    // request.add(utf8.encode(payload));
    // //request.addStream(_inputStream); //可以直接添加输入流
    // httpClient.close();
    uri = Uri.http("127.0.0.1:5000", "/Test/Uas/SetUASPositionlog");
    double x = 3.0;
    double y = 4.0;

    String jsonString = "[{\"x\":\"" +
        x.toString() +
        "\",\"y\":\"" +
        y.toString() +
        "\",\"id\":\"robot\"}]";
    Response res = await post(
      uri,
      body: jsonString,
      headers: {"Content-Type": "application/json"},
    );
    if (res.statusCode == 200) {
      return res.body;
    } else {
      return "Error: " + res.body;
    }
  }
}
